from flask import render_template, flash, redirect, session, url_for, request, g
from myapp import app, login_manager, db, models
from form import *
from flask.ext.login import login_required, login_user, logout_user, current_user
import hashlib
from db_add import *

@app.route('/')
def home():
	return redirect('signin')
	# return redirect('profile')

# @app.route('/login', methods = ['GET', 'POST'])
# def login():
# 	if g.user is not None and g.user.is_authenticated():
# 		return redirect('index')
# 	form = LoginForm()
# 	if form.validate_on_submit():
# 		u = models.User.query.get(form.uid.data)
# 		if u is not None:
# 			m = hashlib.md5()
# 			m.update(form.password.data)
# 			if m.hexdigest() == u.password:
# 				login_user(u)
# 				return redirect('index')
# 		return redirect('login')
# 	return render_template('login.html', title = 'Sign In', form = form, styleclass='topcontainer')

# @app.route('/index')
# @login_required
# def index():
# 	user = { 'name': g.user.name }
# 	posts = g.user.posts.all()
# 	return render_template('index.html', title = 'Home', user = user, posts = posts, styleclass = 'topcontainer-index')

# @app.before_request
# def before_request():
#     g.user = current_user

# @login_manager.user_loader
# def load_user(userid):
# 	return models.User.query.get(userid)

# @app.route('/logout')
# def logout():
# 	if g.user is None:
# 		return redirect('login')
# 	logout_user()
# 	return redirect('login')

# @app.route('/signup', methods = ['GET', 'POST'])
# def signup():
# 	form = SignupForm()
# 	if form.validate_on_submit():
# 		u = models.User.query.get(form.uid.data)
# 		if u is None:
# 			print 'hello'
# 			if form.password.data == form.cpassword.data:
# 				v = models.User(form.uid.data, form.name.data, form.email.data, form.password.data)
# 				db.session.add(v)
# 				db.session.commit()
# 				login_user(v)
# 			else:
# 				form = SignupForm()
# 				return render_template('signup.html', title = 'Sign Up', form = form, styleclass = 'topcontainer', errorResponse = 'Passwords did not match!')
# 			return redirect('index')
# 		else:
# 			form = SignupForm()
# 			return render_template('signup.html', title = 'Sign Up', form = form, styleclass = 'topcontainer', errorResponse = "User already exists!")
# 	return render_template('signup.html', title = 'Sign Up', form = form, styleclass = 'topcontainer')

# @app.route('/profile')
# @login_required
# def profile():
# 	user = g.user;
# 	posts = models.Post.query.get(user.uid)
# 	return render_template('profile.html', title='Profile', styleclass = 'topcontainer-index')

# @app.route('/group')
# @login_required
# def group():
# 	user = { 'name': 'The group page' }
# 	posts = []
# 	return render_template('group.html', title = 'Group', user = user, posts = posts, styleclass = 'topcontainer-index')

@app.route('/signin', methods = ['GET', 'POST'])
def login():
	try:
		if g.user is not None and g.user.is_authenticated():
			if g.user.usertype == 'admin':
				return redirect('admin')
			return redirect('profile')
	except:
		pass
	form = LoginForm()
	if form.validate_on_submit():
		u = models.User.query.get(form.uid.data)
		if u is not None:
			m = hashlib.md5()
			m.update(form.password.data)
			if m.hexdigest() == u.password:
				login_user(u)
				if u.uid == 'admin':
					return redirect('admin')
				return redirect('profile')
		error = {"title":"Invalid uid/password", "message":"The uid or password is invalid. Please check again"}
		return render_template('signin.html', title = 'Sign In', form = form, styleclass='topcontainer', error = error)
	return render_template('signin.html', title = 'Sign In', form = form, styleclass='topcontainer')

@app.route('/profile')
@login_required
def profile():
	if g.user.usertype == 'admin':
		return redirect('admin')
	notifications = g.user.notifications.filter_by(unread=True).all()
	if g.user.group != None:
		advisor = models.User.query.get(g.user.group.auid)
	else:
		advisor = None
	return render_template('profile.html', title = 'Profile', user = g.user, notifications = notifications, advisor = None)

@app.before_request
def before_request():
	g.user = current_user

@login_manager.user_loader
def load_user(userid):
	return models.User.query.get(userid)

@app.route('/logout')
def logout():
	if g.user is None:
		return redirect('signin')
	for notification in g.user.notifications.filter_by(unread=True).all():
		notification.unread = False
	db.session.commit()
	logout_user()
	return redirect('signin')

@app.route('/newpost', methods = ['GET', 'POST'])
@login_required
def newpost():
	if g.user.usertype == 'admin':
		return redirect('admin')
	form = NewPost(request.form, csrf_enabled = False)
	if request.method == "POST" and form.validate():
		addpost(form.title.data, form.body.data, dt.utcnow(), g.user.uid, g.user.group.id)
		for member in g.user.group.members.all():
			addnotification(form.title.data, form.body.data, 'New Post', dt.utcnow(), member.uid)
		return redirect('/group')
	return render_template('newpost.html', title = 'New Post', form = form, user = g.user)

@app.route('/members')
@login_required
def members():
	if g.user.usertype == 'admin':
		return redirect('admin')
	return render_template('members.html', title = 'Members of Group', user = g.user)

@app.route('/advisor')
@login_required
def faculty():
	if g.user.usertype == 'admin':
		return redirect('admin')
	advisor = models.User.query.get(g.user.group.auid)
	return render_template('faculty.html', title = 'Faculty Advisor Page', user = g.user, advisor = advisor)

@app.route('/group')
@login_required
def group():
	if g.user.usertype == 'admin':
		return redirect('admin')
	return render_template('fgroup.html', title = 'Group Page', user = g.user, group = g.user.group)

@app.route('/showpost', methods = ['GET', 'POST'])
@login_required
def postpage():
	if g.user.usertype == 'admin':
		return redirect('admin')
	id = request.args['id']
	post = models.Post.query.get(id)
	form = NewReply(request.form, csrf_enabled = False)
	if request.method == "POST" and form.validate():
		addreply(form.body.data, dt.utcnow(), post.id, g.user.uid)
		return redirect('/showpost?id=' + str(post.id))
	return render_template('postpage.html', title = post.title, post = post, form = form, user = g.user)

@app.route('/newissue', methods = ['GET', 'POST'])
@login_required
def newissue():
	if g.user.usertype == 'admin':
		return redirect('admin')
	form = NewIssue(request.form, csrf_enabled = False)
	if request.method == "POST" and form.validate():
		addissue(form.title.data, form.body.data, dt.utcnow(), g.user.uid, g.user.group.id)
		return redirect('/issues')
	return render_template('newissue.html', title = 'New Post', form = form, user = g.user)

@app.route('/issues')
@login_required
def issues():
	if g.user.usertype == 'admin':
		return redirect('admin')
	return render_template('issues.html', title = 'Issues Page', user = g.user, group = g.user.group)

@app.route('/issue', methods = ['GET', 'POST'])
@login_required
def issuepage():
	if g.user.usertype == 'admin':
		return redirect('admin')
	id = request.args['id']
	issue = models.Issue.query.get(id)
	form = NewResponse(request.form, csrf_enabled = False)
	if request.method == "POST" and form.validate():
		addresponse(form.body.data, dt.utcnow(), issue.id, g.user.uid)
		return redirect('/issue?id=' + str(issue.id))
	return render_template('issue.html', title = issue.title, issue = issue, form = form, user = g.user)

@app.route('/notifications', methods = ['GET', 'POST'])
@login_required
def notificationpage():
	if g.user.usertype == 'admin':
		return redirect('admin')
	form = NewNotification(request.form, csrf_enabled = False)
	if request.method == "POST" and form.validate():
		for member in g.user.group.members.filter_by(usertype='student'):
			addnotification(form.title.data, form.body.data, form.clas.data, dt.utcnow(), member.uid)
		return redirect('notifications')
	return render_template('notifications.html', title = 'Notifications', form = form, user = g.user)

@app.route('/alumni')
@login_required
def showalumni():
	if g.user.usertype == 'admin':
		return redirect('admin')
	alumni = models.Alumni.query.all()
	return render_template('alumni.html', title = 'Alumni', alumni = alumni, user = g.user)

@app.route('/settings', methods = ['GET', 'POST'])
@login_required
def settingspage():
	form = SettingsForm(request.form, csrf_enabled = False)
	if request.method == "POST" and form.validate():
		m = hashlib.md5()
		m.update(form.oldpasswd.data)
		if g.user.password == m.hexdigest() and form.newpasswd.data == form.cnewpasswd.data:
			m = hashlib.md5()
			m.update(form.newpasswd.data)
			g.user.password = m.hexdigest()
		else:
			return render_template('settings.html', title = 'Settings', user = g.user, form = form, error = {'title':'Invalid Details','message':'Check details again.'})
		db.session.commit()
		return redirect('admin-users')
	return render_template('settings.html', title = 'Settings', user = g.user, form = form)






# -------------------------------------- ADMIN SIDE ------------------------------------------#







@app.route('/admin')
@login_required
def adminhome():
	if g.user.usertype != 'admin':
		return redirect('/')
	return render_template('admin.html', title = 'Admin', admin = g.user)

@app.route('/admin-users', methods = ['GET', 'POST'])
@login_required
def adminusers():
	if g.user.usertype != 'admin':
		return redirect('/')
	getuserform = AdminGetUser(request.form, csrf_enabled = False)
	newuserform = AdminUpdateUser(request.form, csrf_enabled = False)
	if request.method == "POST" and newuserform.validate():
		user = models.User.query.get(getuserform.uid.data)
		user.uid = newuserform.uid.data
		user.name = newuserform.name.data
		user.email = newuserform.email.data
		user.gid = newuserform.gid.data
		user.usertype = newuserform.usertype.data
		print newuserform.passwd.data
		if newuserform.passwd.data != "":
			m = hashlib.md5()
			m.update(newuserform.passwd.data)
			user.password = m.hexdigest()
		db.session.commit()
		return redirect('admin-users')
	if request.method == "POST" and getuserform.validate():
		user = models.User.query.get(getuserform.uid.data)
		if user == None:
			return render_template('admin-users.html', title = 'Admin', admin = g.user, getuserform = getuserform, error = {'title':'Invalid User','message':'No such User in database'})
		return render_template('admin-users.html', title = 'Admin', admin = g.user, user = user, getuserform = getuserform, newuserform = newuserform)
	return render_template('admin-users.html', title = 'Admin', admin = g.user, getuserform = getuserform)

@app.route('/admin-newuser', methods = ['GET', 'POST'])
@login_required
def adminnewusers():
	if g.user.usertype != 'admin':
		return redirect('/')
	form = AdminAddUser(request.form, csrf_enabled = False)
	if request.method == 'POST' and form.validate():
		u = models.User.query.get(form.uid.data)
		if u is None:
			if form.passwd.data == form.cpasswd.data:
				v = adduser(form.uid.data, form.passwd.data, form.name.data, form.email.data, form.dept.data, form.usertype.data, dt.utcnow())
				if form.gid.data != "":
					v.gid = form.gid.data
					db.session.commit()
			else:
				form = AdminAddUser(request.form, csrf_enabled = False)
				return render_template('admin-newuser.html', title = 'Sign Up', form = form, error = {'title':'Invalid Password','message':'Passwords did not match!'}, admin = g.user)
			return redirect('admin-users')
		else:
			form = AdminAddUser(request.form, csrf_enabled = False)
			return render_template('admin-newuser.html', title = 'Sign Up', form = form, error = {'title':'Invalid User','message':'User already exists!'}, admin = g.user)
	return render_template('admin-newuser.html', title = 'Sign Up', form = form, admin = g.user)

@app.route('/admin-groups')
@login_required
def admingroups():
	if g.user.usertype != 'admin':
		return redirect('/')
	groups = models.Group.query.all()
	def getItem(auid):
		if auid != None:
			return models.User.query.get(group.auid)
		else:
			return None
	advisors = [getItem(group.auid) for group in groups]
	print advisors
	return render_template('admin-groups.html', title = 'Groups', admin = g.user, grouplist = zip(groups, advisors))

@app.route('/admin-groupsettings', methods = ['GET', 'POST'])
@login_required
def admingroupsettings():
	if g.user.usertype != 'admin':
		return redirect('/')
	id = int(request.args['id'])
	form = AdminGroupSettings()
	group = models.Group.query.get(id)
	if group.auid != None:
		advisor = models.User.query.get(group.auid)
	else:
		advisor = None
	members = group.members.filter_by(usertype='student')
	if request.method == 'POST' and form.validate():
		for member in members:
			member.gid = None
		for memberid in form.members.data.split(' '):
			member = models.User.query.get(memberid)
			member.gid = id
		if advisor != None:
			advisor.gid = None
		newadvisor = models.User.query.get(form.advisor.data)
		newadvisor.gid = id
		group.auid = newadvisor.uid
		db.session.commit()
		return redirect('/admin-groups')
	form.members.data = " ".join([str(member.uid) for member in members])
	form.advisor.data = str(group.auid)
	if group.auid == None:
		form.advisor.data = ""
	return render_template('admin-groupsettings.html', title = 'Group Settings', admin = g.user, form = form, id = id)

@app.route('/admin-newgroup', methods = ['GET', 'POST'])
@login_required
def adminnewgroup():
	print '@@@@@'
	if g.user.usertype != 'admin':
		return redirect('/')
	group = addgroup(None)
	return redirect('/admin-groups')

@app.route('/admin-deletegroup', methods = ['GET', 'POST'])
@login_required
def admindeletegroup():
	if g.user.usertype != 'admin':
		return redirect('/')
	id = int(request.args['id'])
	db.session.delete(models.Group.query.get(id))
	db.session.commit()
	return redirect('/admin-groups')

@app.route('/admin-deleteuser', methods = ['GET', 'POST'])
@login_required
def admindeleteuser():
	if g.user.usertype != 'admin':
		return redirect('/')
	uid = request.args['uid']
	db.session.delete(models.User.query.get(uid))
	db.session.commit()
	return redirect('/admin-users')

@app.route('/admin-alumni', methods = ['GET', 'POST'])
@login_required
def adminalumni():
	if g.user.usertype != 'admin':
		return redirect('/')
	getuserform = AdminGetUser(request.form, csrf_enabled = False)
	alumniform = AdminAlumni(request.form, csrf_enabled = False)
	alumnilist = models.Alumni.query.all()
	if request.method == "POST" and getuserform.validate():
		alumni = models.Alumni.query.get(getuserform.uid.data)
		if alumni == None:
			return render_template('admin-alumni.html', title = 'Alumni DB', admin = g.user, getuserform = getuserform, error = {'title':'Invalid Entry','message':'No such Alumni in database'}, alumniform = alumniform, alumnilist = alumnilist)
		alumniform.misc.data = alumni.misc
		return render_template('admin-alumni.html', title = 'Alumni DB', admin = g.user, alumni = alumni, getuserform = getuserform, alumniform = alumniform, alumnilist = [])
	if request.method == "POST" and alumniform.validate():
		alumni = models.Alumni.query.get(alumniform.id.data)
		alumni.name = alumniform.name.data
		alumni.email = alumniform.email.data
		alumni.dept = alumniform.dept.data
		alumni.batch = alumniform.batch.data
		alumni.misc = alumniform.misc.data
		db.session.commit()
		return redirect('admin-alumni')
	return render_template('admin-alumni.html', title = 'Alumni DB', admin = g.user, getuserform = getuserform, alumniform = alumniform, alumnilist = alumnilist)

@app.route('/admin-addalumni', methods = ['GET', 'POST'])
@login_required
def adminaddalumni():
	if g.user.usertype != 'admin':
		return redirect('/')
	form = AdminAlumni(request.form, csrf_enabled = False)
	if request.method == 'POST' and form.validate():
		alumnus = models.Alumni(form.name.data, form.email.data, form.dept.data, form.batch.data, form.misc.data)
		db.session.add(alumnus)
		db.session.commit()
		return redirect('admin-alumni')
	return render_template('admin-addalumni.html', title = 'Alumni', alumniform = form, admin = g.user)

@app.route('/admin-deletealumni', methods = ['GET', 'POST'])
@login_required
def admindeletealumni():
	if g.user.usertype != 'admin':
		return redirect('/')
	id = int(request.args['uid'])

	alumni = models.Alumni.query.get(id)
	if alumni != None:
		db.session.delete(alumni)
		db.session.commit()
		return redirect('/admin-alumni')
	return render_template('admin-addalumni.html', title = 'Alumni', alumniform = form, admin = g.user)