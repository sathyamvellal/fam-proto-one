from flask.ext.wtf import Form, TextField, TextAreaField, BooleanField, RadioField
from flask.ext.wtf import Required

class LoginForm(Form):
	uid = TextField('UID: ', validators = [Required()])
	password = TextField('Password: ', validators = [Required()])
	# remember_me = BooleanField('remember_me', default = False)

class SignupForm(Form):
	name = TextField('Name: ', validators = [Required()])
	usn = TextField('UID: ', validators = [Required()])
	email = TextField('Email: ', validators = [Required()])
	password = TextField('Password: ', validators = [Required()])
	cpassword = TextField('Confirm Password: ', validators = [Required()])

class NewPost(Form):
	title = TextField('Title: ', validators = [Required()])
	body = TextAreaField('Body: ', validators = [Required()])

class NewReply(Form):
	body = TextAreaField('Body: ', validators = [Required()])

class NewIssue(Form):
	title = TextField('Title: ', validators = [Required()])
	body = TextAreaField('Body: ', validators = [Required()])

class NewResponse(Form):
	body = TextAreaField('Body: ', validators = [Required()])

class NewNotification(Form):
	title = TextField('Title: ', validators = [Required()])
	body = TextAreaField('Body: ', validators = [Required()])
	clas = TextField('Class: ', validators = [Required()])

class AdminGetUser(Form):
	uid = TextField('UID: ', validators = [Required()])

class AdminUpdateUser(Form):
	uid = TextField('UID: ', validators = [Required()])
	name = TextField('Name: ', validators = [Required()])
	email = TextField('Email: ', validators = [Required()])
	passwd = TextField('Password: ')
	dept = TextField('Dept: ', validators = [Required()])
	gid	= TextField('Group ID: ')
	usertype = RadioField('User Type', choices=[('student','Student'),('advisor', 'Faculty Advisor')], validators = [Required()])

class AdminAddUser(Form):
	uid = TextField('UID: ', validators = [Required()])
	name = TextField('Name: ', validators = [Required()])
	email = TextField('Email: ', validators = [Required()])
	dept = TextField('Dept: ', validators = [Required()])
	passwd = TextField('Password: ', validators = [Required()])
	cpasswd = TextField('Confirm Password: ', validators = [Required()])
	gid	= TextField('Group ID: ', validators = [Required()])
	usertype = RadioField('User Type', choices=[('student','Student'),('advisor', 'Faculty Advisor')], validators = [Required()])

class AdminGroupSettings(Form):
	members = TextAreaField('Members: ')
	advisor = TextField('Advisor: ')

class AdminAlumni(Form):
	id = TextField('ID: ')
	name = TextField('Name: ', validators = [Required()])
	email = TextField('Email: ', validators = [Required()])
	dept = TextField('Dept: ', validators = [Required()])
	batch = TextField('Batch :', validators = [Required()])
	misc = TextAreaField('Misc Info: ')

class SettingsForm(Form):
	oldpasswd = TextField('Old Password: ', validators = [Required()])
	newpasswd = TextField('New Password: ', validators = [Required()])
	cnewpasswd = TextField('Confirm New Password: ', validators = [Required()])