from myapp import db
import hashlib

class User(db.Model):
	__tablename__ = 'User'
	# id = db.Column(db.Integer, primary_key = True)
	uid = db.Column(db.String(10), index = True, primary_key = True)
	name = db.Column(db.String(64), index = True)
	email = db.Column(db.String(120), index = True, unique = True)
	password = db.Column(db.String(32), index = True)
	dept = db.Column(db.String(32), index = True)
	last_login = db.Column(db.DateTime, index = True)
	posts = db.relationship('Post', backref = db.backref('user', lazy = 'joined'), lazy = 'dynamic')
	replies = db.relationship('Reply', backref = db.backref('user', lazy = 'joined'), lazy = 'dynamic')
	issues = db.relationship('Issue', backref = db.backref('user', lazy = 'joined'), lazy = 'dynamic')
	responses = db.relationship('Response', backref = db.backref('user', lazy = 'joined'), lazy = 'dynamic')
	notifications = db.relationship('Notifications', backref = db.backref('user', lazy = 'joined'), lazy = 'dynamic')
	gid = db.Column(db.Integer, db.ForeignKey('Group.id'));
	usertype = db.Column(db.String(7), index = True)

	def __init__(self, uid, password, name, email, dept, usertype, last_login):
		self.uid = uid
		self.name = name
		self.email = email
		self.dept = dept
		self.gid = None
		self.last_login = last_login
		self.info = {}
		self.usertype = usertype
		m = hashlib.md5()
		m.update(password)
		self.password = m.hexdigest()

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.uid)
	
	def __repr__(self):
		return '<User %r>' + (self.name)
		
class Post(db.Model):
	__tablename__ = 'Post'
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.Text)
	body = db.Column(db.Text)
	timestamp = db.Column(db.DateTime)
	uid  = db.Column(db.String(10), db.ForeignKey('User.uid'))
	gid  = db.Column(db.Integer, db.ForeignKey('Group.id'))
	replies = db.relationship('Reply', backref = db.backref('post', lazy = 'joined'), lazy = 'dynamic')

	def __init__(self, title, body, timestamp, uid, gid):
		self.title = title
		self.body = body
		self.timestamp = timestamp
		self.uid = uid
		self.gid = gid

	def __repr__(self):
		return '<Post %r>' % (self.body)

class Reply(db.Model):
	__tablename__ = 'Reply'
	id = db.Column(db.Integer, primary_key = True)
	body = db.Column(db.Text)
	timestamp = db.Column(db.DateTime)
	pid = db.Column(db.Integer, db.ForeignKey('Post.id'))
	uid = db.Column(db.String(10), db.ForeignKey('User.uid'))

	def __init__(self, body, timestamp, pid, uid):
		self.body = body
		self.timestamp = timestamp
		self.pid = pid
		self.uid = uid

class Group(db.Model):
	__tablename__ = 'Group'
	id = db.Column(db.Integer, primary_key = True)
	auid = db.Column(db.String(10), index = True)
	members = db.relationship('User', backref = db.backref('group', lazy = 'joined'), lazy = 'dynamic')
	posts = db.relationship('Post', backref = db.backref('group', lazy = 'joined'), lazy = 'dynamic')
	issues = db.relationship('Issue', backref = db.backref('group', lazy = 'joined'), lazy = 'dynamic')

	def __init__(self, auid):
		self.auid = auid
		self.advisor = None

class Notifications(db.Model):
	__tablename__ = 'Notifications'
	id = db.Column(db.Integer, primary_key = True)
	subject = db.Column(db.Text)
	body = db.Column(db.Text)
	clas = db.Column(db.String(32))
	timestamp = db.Column(db.DateTime)
	unread = db.Column(db.Boolean)
	uid  = db.Column(db.String(10), db.ForeignKey('User.uid'))

	def __init__(self, subject, body, clas, timestamp, uid):
		self.subject = subject
		self.body = body
		self.clas = clas
		self.timestamp = timestamp
		self.uid = uid
		self.unread = True

	def __repr__(self):
		return '<Post %r>' % (self.body)

class Issue(db.Model):
	__tablename__ = 'Issue'
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.Text)
	body = db.Column(db.Text)
	timestamp = db.Column(db.DateTime)
	uid  = db.Column(db.String(10), db.ForeignKey('User.uid'))
	gid  = db.Column(db.Integer, db.ForeignKey('Group.id'))
	responses = db.relationship('Response', backref = db.backref('issue', lazy = 'joined'), lazy = 'dynamic')

	def __init__(self, title, body, timestamp, uid, gid):
		self.title = title
		self.body = body
		self.timestamp = timestamp
		self.uid = uid
		self.gid = gid

	def __repr__(self):
		return '<Post %r>' % (self.body)

class Response(db.Model):
	__tablename__ = 'Response'
	id = db.Column(db.Integer, primary_key = True)
	body = db.Column(db.Text)
	timestamp = db.Column(db.DateTime)
	iid = db.Column(db.Integer, db.ForeignKey('Issue.id'))
	uid = db.Column(db.String(10), db.ForeignKey('User.uid'))

	def __init__(self, body, timestamp, iid, uid):
		self.body = body
		self.timestamp = timestamp
		self.iid = iid
		self.uid = uid

class Alumni(db.Model):
	__tablename__ = 'Alumni'
	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(64), index = True)
	email = db.Column(db.String(120), index	= True)
	batch = db.Column(db.String(4), index = True)
	dept = db.Column(db.String(32), index = True)
	misc = db.Column(db.Text, index = True)

	def __init__(self, name, email, batch, dept, misc):
		self.name = name
		self.email = email
		self.batch = batch
		self.dept = dept
		self.misc = misc