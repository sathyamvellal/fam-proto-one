#!flask/bin/python2.7
activate_this = '/srv/http/flasktest/flask/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
import sys
sys.path.insert(0, '/srv/http/flasktest/')
from myapp import app as application
