#!flask/bin/python2.7 

from myapp import db, models
from datetime import datetime as dt

def datenow():
	date = dt.date(dt.today())
	month = {1: 'January',
			 2: 'February',
			 3: 'March',
			 4: 'April',
			 5: 'May',
			 6: 'June',
			 7: 'July',
			 8: 'August',
			 9: 'September',
			 10: 'October',
			 11: 'November',
			 12: 'December'
			}
	return "" + str(date.day) + "-" + month[date.month] + "-" + str(date.year)

def timenow():
	time = dt.time(dt.today())
	hour = time.hour
	minute = time.minute
	second = time.second;
	if second < 10:
		second = "0" + str(second)
	else:
		second = str(second)

	if minute < 10:
		minute= "0" + str(minute)
	else:
		minute = str(minute)

	if hour< 10:
		hour = "0" + str(hour)
	else:
		hour = str(hour)
	return "" + hour + ":" + minute + ":" + second

def adduser(usn, password, name, email, dept, usertype, last_login):
	user = models.User(usn, password, name, email, dept, usertype, last_login)
	db.session.add(user)
	db.session.commit()
	return user

def addpost(title, body, timestamp, uid, gid):
	post = models.Post(title, body, timestamp, uid, gid)
	db.session.add(post)
	db.session.commit()
	return post

def addreply(body, timestamp, pid, uid):
	reply = models.Reply(body, timestamp, pid, uid)
	db.session.add(reply)
	db.session.commit()
	return reply

def addissue(title, body, timestamp, uid, gid):
	issue = models.Issue(title, body, timestamp, uid, gid)
	db.session.add(issue)
	db.session.commit()
	return issue

def addresponse(body, timestamp, iid, uid):
	response = models.Response(body, timestamp, iid, uid)
	db.session.add(response)
	db.session.commit()
	return response

def addnotification(subject, body, clas, timestamp, uid):
	notification = models.Notifications(subject, body, clas, timestamp, uid)
	db.session.add(notification)
	db.session.commit()
	return notification

def addgroup(auid):
	group = models.Group(auid)
	db.session.add(group)
	db.session.commit()
	return group

def adddefault():
	f1 = adduser('f1id', 'qwerty', 'Faculty One', 'faculty.one@pes.edu', 'CSE', 'advisor', dt.utcnow())
	u1 = adduser('1PI10CS001', 'foobar', 'Student One', 'student.one@gmail.com', 'CSE', 'student', dt.utcnow())
	f2 = adduser('f2id', 'qwerty', 'Faculty Two', 'faculty.two@pes.edu', 'CSE', 'advisor', dt.utcnow())
	u2 = adduser('1PI10CS002', 'foobar', 'Student Two', 'student.two@gmail.com', 'CSE', 'student', dt.utcnow())
	u3 = adduser('1PI10CS003', 'foobar', 'Student Three', 'student.three@gmail.com', 'CSE', 'student', dt.utcnow())
	u4 = adduser('1PI10CS004', 'foobar', 'Student Four', 'student.four@gmail.com', 'CSE', 'student', dt.utcnow())
	u5 = adduser('1PI10CS005', 'foobar', 'Student Five', 'student.five@gmail.com', 'CSE', 'student', dt.utcnow())
	u6 = adduser('1PI10CS006', 'foobar', 'Student Six', 'student.six@gmail.com', 'CSE', 'student', dt.utcnow())
	ad = adduser('admin', 'admin', 'PES Admin', 'fam.admin@pes.edu', 'CSE', 'admin', dt.utcnow())
	g1 = addgroup(f1.uid)
	g2 = addgroup(f2.uid)
	addtogroup(f1, g1.id)
	addtogroup(u1, g1.id)
	addtogroup(u2, g1.id)
	addtogroup(u3, g1.id)
	addtogroup(f2, g2.id)
	addtogroup(u4, g2.id)
	addtogroup(u5, g2.id)
	addtogroup(u6, g2.id)

def addtogroup(user, gid):
	user.gid = gid
	db.session.commit()